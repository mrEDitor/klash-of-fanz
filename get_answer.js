var urlLocation = "https://klash-of-klans.ru/server/connector_get_answer.php?jsoncallback=?";
var sNameQuestion;
var isMulti;
var arQuestionJson = {};
var index;

$( document ).ready(function() {
	$('a.continue-button').click(page_reload);
});


function setRadioTraps() {
	$('span.left>input').mousedown(startRecord);
}

function page_reload() {
	setTimeout(setRadioTraps, 5000);
}
    
function startRecord(e) {
	if (e.button == 0) return true;
	$('span.left>input').prop("checked", false);

    /* ТЕКСТ ВОПРОСА */
    sNameQuestion = document.querySelector('div.question').firstChild.data;
    sNameQuestion = sNameQuestion.trim();
    sNameQuestion = sNameQuestion.replace(/\r|\n/g, '');
    sNameQuestion = sNameQuestion.replace(/ +/g," ");


    /* ТИП КНОПКИ */
    if ($('.tip').text() == "(Отметьте один правильный вариант ответа.)") {
        isMulti = 0;    //radio
    }
    else {
        isMulti = 1; //checkbox
    }

    /* СПИСОК ОТВЕТОВ */
    answ = document.querySelectorAll('div.wrapper');

    for (i = 0; i < answ.length; i++) {
        index = answ[i].firstChild.firstChild.firstElementChild.getAttribute("id");
        index = index.trim();
        index = index.replace(/\r|\n/g, '');
        index = index.replace(/ +/g," ");
        // создание ассоциативного массива с ответами
        arQuestionJson[index] = answ[i].firstChild.lastChild.textContent;
        arQuestionJson[index] = arQuestionJson[index].trim(); // убрать лишние пробелы с концов
        arQuestionJson[index] = arQuestionJson[index].replace(/\r|\n/g, '');
        arQuestionJson[index] = arQuestionJson[index].replace(/ +/g," ");
    }
    arQuestion = JSON.stringify(arQuestionJson);
    // console.log(arQuestion);

    addQuestionToServer(sNameQuestion, arQuestion, isMulti);

    $("a.ajax-command-button")[3].onclick = page_reload;


    function addQuestionToServer(strNameQuestion, arQuestion, isMulti) {

        $.getJSON(urlLocation,
            {
                strNameQuestion: strNameQuestion,
                arQuestion: arQuestion,
                isMulti: isMulti
            },
            function (data) {

            });
    }
    
    return false;
}
